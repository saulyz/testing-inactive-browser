// using momentjs
let logContainer,
  intervalIdOnFocus, 
  intervalIdOnBlur;

function clearIntervals() {
  clearInterval(intervalIdOnBlur);
  clearInterval(intervalIdOnFocus);
}

function focusHandler() {
  clearIntervals();
  intervalIdOnFocus = setInterval(() => {
    logToHtmlContainer('focused');
  }, 1000);
}

function blurHandler() {
  clearIntervals();
  intervalIdOnBlur = setInterval(() => {
    logToHtmlContainer('blured');
  }, 1000);
}

function logToHtmlContainer(text) {
  logContainer.insertAdjacentHTML('afterbegin', logMessageHtml(text));
}

function logMessageHtml(text) {
  let classList = 'message message--' + text;
  let message = 'browser is ' + text;
  let timeStamp = moment().format('YYYY-MM-DD LTS');
  return '<div><span class="' + classList + '">' + message + '</span> <span class="timestamp">' + timeStamp + '</span></div>';
}

document.addEventListener("DOMContentLoaded", () => {
  console.log('starting scripts');
  logContainer = document.getElementById("log");
  
  window.addEventListener('focus', focusHandler);
  window.addEventListener('blur', blurHandler);
  // start with focusHandler
  focusHandler();
});
